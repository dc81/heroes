from flask import Flask, render_template, json
from flaskext.mysql import MySQL

mysql = MySQL()
app = Flask(__name__)
app.config.from_pyfile('database.cfg')
app.config.from_pyfile('dbhost.cfg')

mysql.init_app(app)

@app.route('/')
def main():
    return render_template('index.html')

@app.route('/showEntries')
def showEntries():
    # connect to mysql
    connection = mysql.connect()
    cursor = connection.cursor()    
    try:
        cursor.execute('''SELECT * FROM wiki''')
        result = cursor.fetchall()
	    
        heroes = []
        for entry in result:
            record = {
                    'id': entry[0],
                    'name': entry[1],
                    'level': entry[2],
            }
            heroes.append(record)		

        return render_template('db.html', heroes=heroes)
    except Exception as e:
        return json.dumps({'error':str(e)})

    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
        cursor.close()
        connection.close()

if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True)
