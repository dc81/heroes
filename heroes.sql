CREATE DATABASE heroes;
CREATE TABLE `heroes`.`wiki` (
      `id` BIGINT NULL AUTO_INCREMENT,
      `name` VARCHAR(45) NULL,
      `level` INT NULL,
      PRIMARY KEY (`id`));

USE heroes;
INSERT INTO `wiki` VALUES (1,'Iron Man', 100);
INSERT INTO `wiki` VALUES (2,'Thor', 110);
INSERT INTO `wiki` VALUES (3,'Captain America', 111);
INSERT INTO `wiki` VALUES (4,'Boba Fett', 80);
INSERT INTO `wiki` VALUES (5,'Din Djarin', 80);
GRANT ALL PRIVILEGES ON heroes.* TO 'heroes'@'%' IDENTIFIED BY '@123456';
GRANT ALL PRIVILEGES ON heroes.* TO 'heroes'@'localhost' IDENTIFIED BY '@123456';