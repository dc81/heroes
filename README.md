# Heroes Application

Heroes simple Flask application that have entries in MySQL/MariaDB database

## Preparation

### Database

Load data with 

```
mysql -u root -p < heroes.sql
```

### Install required packages in Alpine Linux

```
sudo apk add py3-pip py3-gunicorn
pip3 install flask==2.3.3 flask-mysql markupsafe
```

### Database configuration

1. Ensure database is up and running
2. Edit ```dbhost.cfg``` and ```database.cfg```

## Run Application

```
python3 app.py
```

## Run on Server

```
gunicorn --bind 0.0.0.0:5000 --daemon wsgi:app
```

## Credit

Thanks as we've use partial code from 
https://github.com/pstauffer/flask-mysql-app

## Authors

Authors: Joe Limwiwatkul
